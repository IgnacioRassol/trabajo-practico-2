#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "libreria_main.h"


void Test_generar_numero_aleatorio(){
    int maximo;

    for(maximo = 1; maximo < 100 ; maximo++){
        assert(generar_numero_aleatorio(maximo) < maximo);
    }
}

void Test_Substrings(){
    assert(Substrings("Murcielago", "lago", 10, 4) == 1);
    assert(Substrings("Murcielago", "Lago", 10, 4) == 0);
    assert(Substrings("Navegacion", "Nave", 10, 4) == 1);
    assert(Substrings("Navegacion", "nave", 10, 4) == 0);
    assert(Substrings("accionar", "accion", 8, 6) == 1);
    assert(Substrings("accionar", "acci�n", 8, 6) == 0);
}

void Test_invertir(){
    char destino[15];
    invertir("camarada", destino);
    assert(strcmp(destino, "adaramac")==0);
    invertir("Neuquen", destino);
    assert(strcmp(destino, "neuqueN")==0);
    invertir("CASA", destino);
    assert(strcmp(destino, "ASAC")==0);
}

void Test_LowerString(){
    char destino[15];
    LowerString("AMIGO", destino);
    assert(strcmp(destino, "amigo")==0);
    LowerString("Neuquen", destino);
    assert(strcmp(destino, "neuquen")==0);
    LowerString("casa", destino);
    assert(strcmp(destino, "casa")==0);
}

void Test_validar_palabra(){
    int largos[10];
    char palabras[100][15];

    assert(validar_palabra("Caminar", palabras, 0, largos) == 1);
    assert(validar_palabra("Ala", palabras, 1, largos) == 0);
    assert(validar_palabra("Vivir", palabras, 1, largos) == 1);
    assert(validar_palabra("Muercielago", palabras, 2, largos) == 1);
    assert(validar_palabra("lago", palabras, 3, largos) == 0);
    assert(validar_palabra("Peron", palabras, 3, largos) == 1);
    assert(validar_palabra("Peronismo", palabras, 4, largos) == 0);
    assert(validar_palabra("ramo", palabras, 4, largos) == 1);
    assert(validar_palabra("omar", palabras, 5, largos) == 0);
    assert(validar_palabra("Computacion", palabras, 5, largos) == 1);
    assert(validar_palabra("Computacion", palabras, 6, largos) == 0);

}

void Test_elegir_palabras(){
    int total_palabras = 10, cantidad_palabras_a_elegir=1;
    char palabras[100][15], palabras_seleccionadas[100][15];

    strcpy(palabras[0],"Caminar");
    strcpy(palabras[1],"Ala");
    strcpy(palabras[2],"Vivir");
    strcpy(palabras[3],"Muercielago");
    strcpy(palabras[4],"Peron");
    strcpy(palabras[5],"ramo");
    strcpy(palabras[6],"Computacion");
    strcpy(palabras[7],"Neuquen");
    strcpy(palabras[8],"casa");
    strcpy(palabras[9],"camarada");

    for(; cantidad_palabras_a_elegir < 10; cantidad_palabras_a_elegir++){

        assert(elegir_palabras(palabras, palabras_seleccionadas, total_palabras, cantidad_palabras_a_elegir) == cantidad_palabras_a_elegir);
    }

    for(; cantidad_palabras_a_elegir < 20; cantidad_palabras_a_elegir++){

        assert(elegir_palabras(palabras, palabras_seleccionadas, total_palabras, cantidad_palabras_a_elegir) == 9);
    }

}

/*Esta funcion, toma un archivo especifico de 330 lineas(no todas contienen palabras).
Checkea que la cantidad de palabras que se toman del archivo sea siempre la misma*/
void Test_Lectura_Archivo_Palabras(){
    int cantidad_palabras, i=0;
    char palabras[330][15];
    cantidad_palabras = Lectura_Archivo_Palabras("testing",palabras);
    for(; i<100;i++){
        assert(cantidad_palabras == Lectura_Archivo_Palabras("testing",palabras));
    }
}

void Test_Escritura_Archivo_Palabras(){
    int cantidad_palabras, i=0;
    char palabras[330][15];
    cantidad_palabras = Lectura_Archivo_Palabras("testing",palabras);
    for(; i<1000;i++){
        Escritura_Archivo_Palabras("SALIDA_testing",palabras,cantidad_palabras);
    }
}

void Iron_Test(){
    int total_palabras, cantidad_palabras_a_elegir, palabras_elegidas, validacion, i;
    char palabras[100][15], palabras_seleccionadas[100][15], entrada[100], salida[100];

    srand(time(NULL));
    strcpy(entrada,"testing2");
    strcpy(salida, "salida_testeo");

    for(i = 0; i < 1000; i++){
        total_palabras = Lectura_Archivo_Palabras(entrada, palabras);

        cantidad_palabras_a_elegir = generar_numero_aleatorio(total_palabras) + 1;
        printf("La cantidad de palabras a elegir es:  %d. \n", cantidad_palabras_a_elegir);


        palabras_elegidas = elegir_palabras(palabras, palabras_seleccionadas, total_palabras, cantidad_palabras_a_elegir);
        if(cantidad_palabras_a_elegir == palabras_elegidas)
            Escritura_Archivo_Palabras(salida, palabras_seleccionadas, palabras_elegidas);
        else
            printf("No se pudo cubrir la cantidad de palabras a generar con las palabras ingresadas originalmente. No se genero ningun archivo de salida.\n");
    }

}


int main(){


    Test_generar_numero_aleatorio();

    Test_Substrings();

    Test_LowerString();

    Test_invertir();

    Test_validar_palabra();

    Test_elegir_palabras();

    Test_Lectura_Archivo_Palabras();

    Test_Escritura_Archivo_Palabras();

    Iron_Test();

    return 0;
}

