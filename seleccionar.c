#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

int generar_numero_aleatorio(int maximo)
{
	return rand() % maximo;
}

void invertir(char fuente[], char destino[]){
	int largo, i = 0;

	for (largo = strlen(fuente)-1; largo > 0; largo--, i++){
		destino[i] = fuente[largo];
	}

	destino[i] = '\0';

}

int Substrings(char Palabra1[], char Palabra2[], int Largo_Pal1, int Largo_Pal2){
	int Salida = 0, Diferencia_largo, i;
	char Cadena_aux_Comparaciones[15];

	if (Largo_Pal1 < Largo_Pal2)
		return Salida;

	else {
		Diferencia_largo = Largo_Pal1 - Largo_Pal2+1;
		for(i = 0; i < Diferencia_largo; i++){

			strncpy(Cadena_aux_Comparaciones, Palabra1, Largo_Pal2);
			Cadena_aux_Comparaciones[Largo_Pal2]='\0';

			if(strcmp (Cadena_aux_Comparaciones,Palabra2) == 0)
				Salida = 1;
			Palabra1++;
		}

	}

	return Salida;
}

int validar_palabra(char palabra_actual[], char palabras[][15], int palabras_puestas, int largos[]){
	int largo, i, salida = 1;
	char inversa[15];

	largo = strlen(palabra_actual);

	if (largo < 4)
		return 0;

	if (!palabras_puestas){
		largos[0] = largo;
		return 1;
	}
	else{
		invertir(palabra_actual, inversa);

		for(i = 0; i < palabras_puestas && salida == 1; i++){
			if(Substrings(palabras[i], palabra_actual, largos[i], largo) ||
				 Substrings(palabra_actual, palabras[i], largo, largos[i]) ||
				 strcmp(palabras[i], inversa) == 0)
					salida = 0;
			}
	}

	largos[i] = largo;
	return salida;
}

int elegir_palabras(char palabras[][15], char palabras_seleccionadas[][15], int total_palabras, int cantidad_palabras_a_elegir)
{
	int indices_posibles[100], largos[100], indices_restantes = total_palabras, indice_elegido, palabra_a_revisar,
		contador_palabras = 0, i;


	for (i = 0; i < total_palabras; i++){
		indices_posibles[i] = i;
	}

	while (contador_palabras < cantidad_palabras_a_elegir && indices_restantes > 0){
		indice_elegido = generar_numero_aleatorio(indices_restantes);
		palabra_a_revisar = indices_posibles[indice_elegido];
		indices_posibles[indice_elegido] = indices_posibles[indices_restantes-1];
		indices_restantes--;

		if (validar_palabra(palabras[palabra_a_revisar], palabras_seleccionadas, contador_palabras, largos)){
			strcpy(palabras_seleccionadas[contador_palabras], palabras[palabra_a_revisar]);
			contador_palabras++;
		}
	}
	if (indices_restantes == 0 && contador_palabras != cantidad_palabras_a_elegir){
		printf("No se pudo cubrir la cantidad de palabras a generar con las palabras ingresadas originalmente. \n");
	}

	return contador_palabras;

}

int Lectura_Archivo_Palabras(char Archivo_Entrada[], char Lista_De_Palabras[][15]){

	FILE *Puntero_Archivo = fopen(Archivo_Entrada, "r");
	int Contador_Palabras = 0;
	char buff[15];

	while(!feof(Puntero_Archivo)){
		fscanf(Puntero_Archivo, "%s", buff);
		strcpy(Lista_De_Palabras[Contador_Palabras],buff);
		if (!feof(Puntero_Archivo))
			Contador_Palabras++;
	}

	return Contador_Palabras;

}

void Escritura_Archivo_Palabras(char Archivo_Escritura[], char Lista_De_Palabras[][15], int cantidad_palabras){

	FILE *Puntero_Archivo = fopen(Archivo_Escritura, "w");
	int Contador_Palabras = 0;
	char buff[15];

	while(Contador_Palabras < cantidad_palabras){

		strcpy(buff,Lista_De_Palabras[Contador_Palabras]);
		fprintf(Puntero_Archivo,"%s\n" , buff);

		Contador_Palabras++;
	}

}



void menu(char entrada[], char salida[]){

	//TODO: validar las entradas y salidas

	printf("Ingrese el nombre del archivo de entrada: \n");
	do {
		scanf ("%[^\n]%*c", entrada);
		if (entrada[0] == '\0')
			printf("Tiene que ingresar un archivo de entrada, intente nuevamente. \n");
	} while(entrada[0] == '\0');


	printf("Ingrese el nombre del archivo de salida: \n");

	do {
		scanf ("%[^\n]%*c", salida);
		if (salida[0] == '\0')
			printf("Tiene que ingresar un archivo de entrada, intente nuevamente. \n");
	} while(salida[0] == '\0');

//    FILE *Puntero_Archivo;
//    if(Puntero_Archivo = fopen(entrada, "r")){
//        fclose(Puntero_Archivo);
//        return 1;
//    }else{
//        return 0;
//    }

}

int main()
{
	int total_palabras, cantidad_palabras_a_elegir, palabras_elegidas;
	char palabras[100][15], palabras_seleccionadas[100][15], entrada[100], salida[100];

	srand(time(NULL));

	menu(entrada, salida);
	total_palabras = Lectura_Archivo_Palabras(entrada, palabras);
	printf("%d\n", total_palabras);

	cantidad_palabras_a_elegir = generar_numero_aleatorio(total_palabras) + 1;
	printf("La cantidad de palabras a elegir es:  %d. \n", cantidad_palabras_a_elegir);


	palabras_elegidas = elegir_palabras(palabras, palabras_seleccionadas, total_palabras, cantidad_palabras_a_elegir);
	Escritura_Archivo_Palabras(salida, palabras_seleccionadas, palabras_elegidas);


	return 0;
}
